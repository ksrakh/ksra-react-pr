import { Component } from "react";
import './Base.css';
import List from '../List/List';


export class Base extends Component {
    render() {
        var kala = [
            {name : 'TV' , price : '270,788 $' , description : 'This is best TV for best people!' },
            {name : 'Vacuum Cleaner' , price : '75,34 $' , description : 'It works best for you!' },
            {name : 'Mobile Phone' , price : '112,0 $' , description : 'Never separate from this phone!' },
            {name : 'Table' , price : '56,45 $' , description : 'Say goodbye to old tables!' },
        ]
        return <div>
            {kala.map((item , index) => {
                return <List name={`${item.name}`} description={`${item.description}`} price={item.price} key={index} />
            })}
        </div> 
    }
}