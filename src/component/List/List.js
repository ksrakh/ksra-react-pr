import { Component } from "react"

export default class List extends Component{
    render() {
        var {name , price , description} = this.props
        return <>
            <h2>{name}</h2>
            <h5>{price}</h5>
            <p>{description}</p>
        </>
    }
}